
package Base;
use v5.10;
use utf8;
use DBI;
use Data::Dumper;
use File::Basename;
my $DB;
my $DBFILE;
#     Автор
#     Название
#     Обложка
#     Аннотация
#     Рейтинг
#     Ссылка на комментарии
#     Язык
#     ISBN
#     Хеш
#     Имя файла
#     Путь к файлу (хранится локально и в поиске не участвует)

sub add_book #HASH, title, lang, summary, cover, filename
{
  $DB or die "database is not opened!\n";
  my $hash=shift;
  my $q=$DB->prepare("SELECT ID FROM 'books' WHERE HASH = ('$hash')");
  $q->execute() or die "can't execute";
  my $str=$q->fetchrow_array();
  my $title=shift;
  if(! defined $str)
  {
    $hash=$DB->quote($hash);
    $title=$DB->quote($title);
    my $lang=$DB->quote(shift);
    my $summary=$DB->quote(shift);
    my $cover=$DB->quote(shift);
    my $local_filename=shift;
    my $filename=$DB->quote(basename($local_filename));
    $local_filename=$DB->quote($local_filename);
    $q=$DB->do("INSERT INTO 'books' (HASH, title,language, summary, cover,filename,local_filename)
    VALUES ($hash,$title,$lang,$summary,$cover,$filename,$local_filename)");
    if(defined $q){
      if($q>0){
        my $q=$DB->prepare("SELECT ID FROM 'books' WHERE HASH = ($hash)");
        $q->execute() or die "can't execute";
        my $str=$q->fetchrow_array();
        if(defined $str){
          print "\t-->--New book added: $title\n";
          return $str;
        }
      }
      return 0;
    }
  }else{
    print "\t--!--Book $title already is in the base\n";
    return $str;
  }
  return 0;
}

sub add_author #first_name, last_name
{
  $DB or die "database is not opened!\n";
  my $first_name=$DB->quote(shift);
  my $last_name=$DB->quote(shift);
  #print "trying to add $first_name $last_name\n";
  my $q=$DB->prepare("SELECT ID FROM 'authors' WHERE first_name = $first_name AND last_name = $last_name");
  return if(not defined $q);
  $q->execute() or die "can't execute";
  my $str=$q->fetchrow_array();
  if(! defined $str)
  {
    $q=$DB->do("INSERT INTO 'authors' (first_name,last_name) VALUES ($first_name,$last_name)");
    if(defined $q)
    {
      if($q>0)
      {
        $q=$DB->prepare("SELECT ID FROM 'authors' WHERE first_name = $first_name AND last_name = $last_name");
        $q->execute() or die "can't execute";
        my $str=$q->fetchrow_array();
        print "\t-->--new author added: $first_name $last_name\n";
        return $str;
      }
    }
  }else{
    print "\t--!--Author $last_name already is in the base\n";
    return $str;
  }
  return 0;
}

sub add_link #author_id, book_id
{
  my $author_id=shift;
  my $book_id=shift;
  $DB or die "database is not opened!\n";
  my $q=$DB->prepare("SELECT author_id FROM 'links' WHERE author_id = '$author_id' AND book_id = '$book_id'");
  $q->execute() or die "can't execute";
  my $str=$q->fetchrow_array();
  if(! defined $str)
  {
    $q=$DB->do("INSERT INTO 'links' (author_id,book_id)
    VALUES ('$author_id','$book_id')");
  }
}

sub del_book #TODO: Add other tables checking and delete unused authors an links.
{
  my $data=shift;
  my $filename=$data->{'filename'};
  my $hash=$data->{'hash'};
  $DB or die "database is not opened!\n";
  if($filename)
  {
    $filename=$DB->quote($filename);
    my $q=$DB->prepare("DELETE FROM 'books' WHERE local_filename = $filename");
    $q->execute() or die "can't execute";
  }
  if($hash)
  {
    $hash=$DB->quote($hash);
    my $q=$DB->prepare("DELETE FROM 'books' WHERE hash = $hash");
    $q->execute() or die "can't execute";
  }
}

sub is_in_base #filename
{
  my $data=shift;
  my $filename=$data->{'filename'};
  my $hash=$data->{'hash'};
  if($filename)
  {
    $filename=$DB->quote($filename);
    my $q=$DB->prepare("SELECT local_filename FROM 'books' WHERE local_filename=$filename");
    $q->execute() or die "can't request filename";
    my $str=$q->fetchrow_array();
    return $str;
  }
  if($hash)
  {
    $hash=$DB->quote($hash);
    my $q=$DB->prepare("SELECT hash FROM 'books' WHERE hash=$hash");
    $q->execute() or die "can't request filename";
    my $str=$q->fetchrow_array();
    return $str;
  }
  return undef;
}

sub search
{
  my $data=shift;
  my $hash=$data->{hash};
  my $title=$data->{title};
  my $first_name=$data->{first_name};
  my $last_name=$data->{last_name};
  my $lang=$data->{lang};
  my $filename=$data->{filename}; # *=all files
  $DB or die "database is not opened!\n";
  $hash and $hash=$DB->quote($hash);
  #$title and $title=$DB->quote("%$title%") and utf8::decode($title);
  #$title = utf8::decode($title);
  $first_name and $first_name=$DB->quote($first_name);# and utf8::decode($first_name);
  $last_name and $last_name=$DB->quote($last_name);# and utf8::decode($last_name);
  $lang and $lang=$DB->quote($lang);
  $filename and $filename=$DB->quote($filename) ;#and utf8::decode($filename);
  #$request="SELECT books.title, GROUP_CONCAT(a2.first_name || ' ' || a2.last_name, ', ') FROM authors INNER JOIN links ON links.author_id = authors.id LEFT JOIN books ON books.id = links.book_id LEFT JOIN links AS l2 ON l2.book_id = books.id LEFT JOIN authors AS a2 ON a2.id = l2.author_id WHERE";
  $request="SELECT DISTINCT books.HASH, books.title,  a2.first_name, a2.last_name, books.language, books.summary, books.filename ".
        "FROM authors INNER JOIN links ON links.author_id = authors.id JOIN books ON books.id = links.book_id ".
        "JOIN links AS l2 ON l2.book_id = books.id JOIN authors AS a2 ON a2.id = l2.author_id WHERE";
  if (defined $filename and $filename eq "'*'")
  {
    $request=~s/books\.filename/books\.local_filename/;
    $request=~s/WHERE$//;
  }
  else{
    $request.=" books.HASH LIKE $hash AND" if(defined $hash and length $hash>10);
    $request.=" books.language LIKE $lang AND" if(defined $lang and length $lang>1);
    $request.=" books.filename LIKE $filename AND" if(defined $filename and length $filename > 2);
    if(length($title)+length($first_name)+length($last_name)>3)
    {
      $request.=" books.title REGEXP '$title' AND" if(defined $title and length($title)>0);
      $request.=" authors.first_name REGEXP $first_name AND" if(defined $first_name and length($first_name)>0);
      $request.=" authors.last_name REGEXP $last_name AND" if(defined $last_name and length($last_name)>0);
    }
    $request=~s/AND$//;
  }
   return if ($request=~/WHERE$|AND$/);
  warn "REQUEST=$request\n";
  #utf8::decode($request);
  my $q=$DB->prepare("$request");

  $q->execute() or die "can't execute";
  my $str;
  my @results;
  my $flag=0;
  my $iflag=0;
  while(my($hash,$title,$first_name, $last_name,$lang,$summary,$filename)=$q->fetchrow_array())
  #while(my($hash,$title,$first_name, $last_name,$lang,$summary,$filename)=$DB->selectcol_arrayref($q, undef, $title))
  {
    $iflag=0;
    #print "$title\n";
    foreach my $i (@results)
    {
      if($i->{'hash'} eq $hash)
      {
        push @{$i->{'authors'}},{first_name=>$first_name,last_name=>$last_name};
        $iflag=1;
        last;
      }
    }
    $str={
      hash=>$hash,
      title=>$title,
      authors=>[
          {first_name=>$first_name,last_name=>$last_name},
      ],
      lang=>$lang,
      #summary=>$summary, #temporary
      summary=>"",
      filename=>$filename
    };
    push @results,$str if(! $iflag);
    $flag=1;
  }
  if(!$flag)
  {
    warn "Nothing found!\n";
    return undef;
  }
  return \@results;
}

sub get_file_name
{
  my $hash=shift;
  $hash or return undef;
  $DB or die "database is not opened!\n";
  $hash and $hash=$DB->quote("$hash");
  my $request="SELECT books.local_filename FROM books WHERE books.HASH = $hash";
  my $q=$DB->prepare("$request");
  $q->execute() or die "can't execute";
  my $filename=$q->fetchrow_array();
  return $filename;
}

sub open_base
{
  $DBFILE=shift;
  $DBFILE or die "Can't access db file\n";
  print "opening the base file $DBFILE\n";
  $DB = DBI->connect("dbi:SQLite:dbname=$DBFILE","","");
  $DB or die "Can't connect to database\n";
  $DB->{sqlite_unicode} = 1;
  $DB->func('regexp', 2, sub {
      my ($regex, $string) = @_;
      $regex=quotemeta $regex;
      return $string =~ /$regex/si if $string;
      return 0;
  }, 'create_function');
  #check tables
  my $query = $DB->prepare("SELECT name FROM sqlite_master WHERE type='table' AND name='books';");
  $query->execute() or die "can't exec";
  if(! defined $query->fetchrow_array())
  {
    print "table is not exist!\nCreating database...";
    my $query=$DB->do("CREATE TABLE 'books' (ID INTEGER PRIMARY KEY AUTOINCREMENT,
          title         TEXT NOCASE,
          cover         TEXT,
          rating        INTEGER,
          comments      TEXT,
          summary       TEXT,
          language      char(5),
          HASH          TEXT UNIQUE,
          filename      TEXT,
          local_filename    TEXT,
          UNIQUE (HASH))");
    $query or die "Can't create table 'books'\n";
    print "ok\n";
  }

  #check tables
  $query = $DB->prepare("SELECT name FROM sqlite_master WHERE type='table' AND name='authors';");
  $query->execute() or die "can't exec";
  if(! defined $query->fetchrow_array())
  {
    print "table is not exist!\nCreating database...";
    my $query=$DB->do("CREATE TABLE 'authors'(ID INTEGER PRIMARY KEY AUTOINCREMENT,
            first_name    TEXT NOCASE,
            last_name   TEXT NOCASE)");
    $query or die "Can't create table 'authors'\n";
    print "ok\n";
  }

  #check tables
  $query = $DB->prepare("SELECT name FROM sqlite_master WHERE type='table' AND name='links';");
  $query->execute() or die "can't exec";
  if(! defined $query->fetchrow_array())
  {
    print "table is not exist!\nCreating database...";
    my $query=$DB->do("CREATE TABLE 'links' (book_id      INTEGER,
              author_id     INTEGER)");
    $query or die "Can't create table 'links'\n";
    print "ok\n";
  }
  #print "Database was opened successfuly\n" if $DB;
}


1;