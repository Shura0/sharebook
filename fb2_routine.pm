use Exception::Class::TryCatch;
use Encode;
use v5.10;
use utf8;

sub parse_fb2 # xml parsing. Is not used (too slow)
{
  $fb2=shift or die "Can't parse fb2\n";
  my @author_firstname;
  my @author_lastname;
  my $lang;
  my $title;
  my $cc;
  my $summary;
  alarm 10;
  eval{
  local $SIG{ALRM}=sub{die "Parsing is too long.\n";};
  my $xp = XML::XPath->new(xml => $fb2);
    my $nodeset = $xp->find('//description/title-info/author'); # find all authors
    foreach my $node ($nodeset->get_nodelist)
  {
        #my $t=XML::XPath::XMLParser::as_string($node);
    #my $p = XML::XPath->new(xml => $t);
    my $fn = $xp->find('./first-name',$node);
    my $ln = $xp->find('./last-name',$node);
    if($fn){push @author_firstname, $fn->string_value;};
    if($ln){push @author_lastname, $ln->string_value;};
  }
  $summary = $xp->find('//description/title-info/annotation'); # find annotation
  $lang=$xp->find('//description/title-info/lang');
  $title=$xp->find('//description/title-info/book-title');
  my $cover=$xp->find('//description/title-info/coverpage/image');
  my $cover_data;
  my $cover_contenttype;
  my $cover_name;
  if(ref $cover eq "XML::XPath::NodeSet")
  {
    foreach my $node ($cover->get_nodelist)
    {
      #print "href:".$node->getAttribute("l:href")."\n";
      my $cn=$node->getAttribute("l:href");
      if($cn eq "")
      {
        $cn=$node->getAttribute("xlink:href");
      }
      $cover_name=substr $cn,1;
    }
  }
  if(0) #get cover image
  {
  my $binary=$xp->find('//binary');
  if(defined $cover_name)
  {
    foreach my $node ($binary->get_nodelist)
    {
      #print "href:".$node->getAttribute("l:href")."\n";
      if($node->getAttribute("id") eq $cover_name)
      {
        $cover_contenttype=$node->getAttribute("content-type");
        $cover_data=$node->string_value;
        last;
      }
    }
  }
  }
  if(!defined $title){die "Title is not defined"};
    my $cc="";
    $cover_contenttype and $cover_data and $cc="$cover_contenttype:$cover_data";
    $cc="";
  };
  err:alarm 0;
  if(!catch my $e)
  {
    return \@author_firstname,\@author_lastname,$lang,$title,$cc,$summary;
  }
  print "Something wrong! Can't parse fb2\n";
  my (@af, @al);
  return @af,@al,"","","","";
}

sub f_parse_fb2 # fast simple parsing. Warning! It isn't safe!
{
   $fb2=shift or die "Can't parse fb2\n";
   my $encoding;
  my @author_firstname;
  my @author_lastname;
  my $lang;
  my $title;
  my $cc;
  my $summary;
   if($fb2=~/\<\?xml([^\>]+?)\>/)
   {
      my $xml=$1;
      if($xml=~/encoding=["'](.+?)["']/)
      {
         $encoding=$1;
      }
   }
   if($fb2=~/\<description\>(.*)\<\/description\>/is)
   {
      my $descr=$1;
      if($descr=~/\<title-info\>(.*)\<\/title-info\>/is)
      {
         my $titleinfo=$1;
         if($titleinfo=~/\<author\>(.*)\<\/author\>/is)
         {
            my $authors=$1;
            while($authors=~/\<first-name\>([^\<]+?)\<\/first-name\>/is)
            {
               my $an=safe_decode($encoding,$1);
               #print "Author: $an\n";
               push @author_firstname, $an;
               my $qm=quotemeta $1;
               $authors=~s/$qm//;
            }
            while($authors=~/\<last-name\>([^\<]+?)\<\/last-name\>/is)
            {
               my $an=safe_decode($encoding,$1);
               push @author_lastname, $an;
               my $qm=quotemeta $1;
               $authors=~s/$qm//;
            }
         }
         if($titleinfo=~/\<book-title\>(.*)\<\/book-title\>/is)
         {
            my $an=safe_decode($encoding,$1);
            $title=$an;
         }
         if($titleinfo=~/\<annotation\>(.+?)\<\/annotation\>/is)
         {
            my $an=safe_decode($encoding,$1);
            $summary=$an;
            $summary=~s/\<[^\>]+?\>//is;
         }
         if($titleinfo=~/\<lang\>(\w=?)\<\/lang\>/is)
         {
            $lang=$1;
         }
      }
   }
   return \@author_firstname,\@author_lastname,$lang,$title,$cc,$summary;
   #return 0;
}

sub safe_decode
{
   my $encoding=shift;
   my $text=shift;
   my $enc=find_encoding($encoding);
   return $text if not $enc;
   return $enc->decode($text);
}

1;