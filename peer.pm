package Peer;
my $IDLE_TIME=10;
use v5.10;
use utf8;
use Encode;
use Term::ANSIColor;
use Term::ANSIColor qw(:constants);
$Term::ANSIColor::AUTORESET=1;
sub new
{
  # TODO: Revise and make it easier
    my $class = shift;
    my $self = {
  _idle => 0,
  _ping_failed=>0, #how many ping are failed
  _inbuffer=>'', #input buffer
  _outbuffer=>'',
  _messages=>[],
  _file=>{ # if file transfer
    _handle=>'',
    _name=>'',
    _size=>'',
    _hash=>'',
    _owner=>'', # Who has the file.
    _data=>""
  },
  _filename=>'',
  _maxsize=>1000000, #maximum buffer size
  _binary=>0, #is binary data (for book transfer)
  _socket=>0, #socket reference
  _status=>0, #are we waiting for answer or not
  _waitforsend=>0, #waiting for sending (socket is not connected yet)
  _close=>0, #we going to close this peer
  _startidletime=>time,
  _bandwidth=>1,
  _canbemaster=>0,
  _version=>1,
  _clients=>'0/0',
  _ip=>0,
  _port=>0,
  _sport=>0,
  _nat=>'true',
  _type=>'client', #client, master, nat, get (file getting).
  _ss=>{}, #statuses array
  _lastsearch=>0 # last search time.
  };
    bless $self, $class;
    return $self;
}

sub ip
{
  my $self=shift;
  my $ip = shift;
   if(defined($ip)){$self->{_ip} = $ip}
  else {return $self->{_ip};}
}

sub port
{
  my $self=shift;
  my $port  = shift;
  if(defined($port)){$self->{_port} = $port}
  else{return $self->{_port}}
}

sub sport
{
  my $self=shift;
  my $sport  = shift;
  if(defined($sport)){$self->{_sport} = $sport}
  else{return $self->{_sport}}
}


sub nat
{
  my $self=shift;
  my $nat = shift;
  if(defined($nat)){$self->{_nat} = $nat}
  else{return $self->{_nat}}
}

sub bandwidth
{
  my $self=shift;
  my $bandwidth = shift;
  if(defined($bandwidth)){$self->{_bandwidth} = $bandwidth}
  else{return $self->{_bandwidth}}
}

sub version
{
  my $self=shift;
  my $version = shift;
  if(defined($version)){$self->{_version} = $version}
  else{return $self->{_version}}
}

sub type # client, master, nat, get (file getting)
{
  my $self=shift;
  my $type= shift;
  if(defined($type)){$self->{_type} = $type}
  else{return $self->{_type}}
}

sub canbemaster
{
  my $self=shift;
  my $type= shift;
  if(defined($type)){$self->{_canbemaster} = $type}
  else{return $self->{_canbemaster}} 
}

sub binary
{
  my $self=shift;
  my $binary= shift;
  if(defined($binary)){
    $self->{_binary} = $binary;
    $self->{_inbuffer}="";
    print "Socket switched to binary mode ($binary)\n";
    binmode $self->{_socket};
    #binmode $self->{_socket}, ":utf8" if($binary==0);
  }
  else{return $self->{_binary}}
}

sub maxsize
{
  my $self=shift;
  my $size= shift;
  if(defined($size)){$self->{_maxsize} = $size}
  else{return $self->{_maxsize}}
}

sub waitingforsend
{
  my $self=shift;
  my $val=shift;
  if(defined($val)){$self->{_waitforsend}=$val}
  else{return $self->{_waitforsend}}
}

# file routine
sub filedescriptor
{
  my $self=shift;
  my $filedescriptor=shift;
  if(defined($filedescriptor)){$self->{_file}->{_handle} = $filedescriptor}
  else{return $self->{_file}->{_handle}}
}

sub filename
{
  my $self=shift;
  my $filename=shift;
  if(defined($filename)){$self->{_file}->{_name} = $filename}
  else{return $self->{_file}->{_name}}
}

sub filehash
{
  my $self=shift;
  my $hash=shift;
  if(defined($hash)){$self->{_file}->{_hash} = $hash}
  else{return $self->{_file}->{_hash}}
}

sub filesize
{
  my $self=shift;
  my $filesize=shift;
  if(defined($filesize)){$self->{_file}->{_size} = $filesize}
  else{return $self->{_file}->{_size}}
}


sub fileowner
{
  my $self=shift;
  my $host=shift;
  if(defined($host)){$self->{_file}->{_owner} = $host}
  else{return $self->{_file}->{_owner}}
}


sub filedata
{
  my $self=shift;
  my $filedata=shift;
  if(defined($filedata)){$self->{_file}->{_data} = $filedata}
  else{return $self->{_file}->{_data}}
}
#end of file routine


sub socket #socket info
{
  my $self=shift;
  my $sock=shift;
  if(defined($sock)){$self->{_socket} = $sock}
  else{return $self->{_socket}}
}

#sub status #status
#{
# $self=shift;
# my $status=shift;
# if(defined $status)
# {
#   $self->{_status}=$status;
#    $self->{'_ss'}->{$status}=time;
# }
# return $self->{_status};
#}

sub add_status #status
{
  $self=shift;
  my $status=shift;
  if(defined $status and $status ne "none")
  {
    $self->{_status}=$status;
    $self->{'_ss'}->{$status}=time;
    #print $self->{_ip}.":".$self->{_sport}." > Status $status added\n";
  }
  return $self->{_status};
}

sub del_status
{
   $self=shift;
   my $s=shift;
   #print $self->{_ip}.":".$self->{_sport}." < Status $s deleted \n";
   delete $self->{'_ss'}->{$s} if $s;
}

#sub statuses #status
#{
# $self=shift;
# return $self->{_ss};
#}


sub get_status
{
  $self=shift;
  my $st=shift;
  $st or return 0;
  return $self->{'_ss'}->{$st};
}

sub utilization #clients amount in format Clients/MAX;
{
  $self=shift;
  my $clients=shift;
  if(defined $clients)
  {
    $self->{_clients}=$clients;
  }
  return $self->{_clients};
}

sub idletime
{
  $self=shift;
  return time-$self->{_startidletime};
}


sub is_closing
{
  $self=shift;
  return $self->{_close};
}

sub close
{
  my $self=shift;
  $self->{_close}=1;
}

sub searchtime
{
  my $self=shift;
  my $t=shift;
  $self->{_lastsearch}=$t if(defined $t);
  return $self->{_lastsearch};
}

sub process #main function
{
  my $self=shift;
  my $s=$self->{'_ss'};
  if($self->{_waitforsend})
  {
    $self->send("","none");
  }
  foreach my $i (keys %{$s})
  {
   next if $i eq 'idle';
   if(time-$s->{$i}>$IDLE_TIME)
    {
      $self->{_close}=1;
      print "A-A-A, found a dreamer. $i, host=".$self->ip().":".$self->sport()."\n";
      last;
    }
  }
  if($self->{_ping_failed}>3)
  {
     print "No 3 ping answer\n";
    $self->{_close}=1;
  }
}


#=============================== BUFFER ROUTINES =============================
sub add_buffer
{
  # TODO: Rewrite json parser.
  my $self=shift;
  my $data=shift;
  $self->{_startidletime}=time;
  if($self->{_binary} and $self->{_file}->{_size}>0)
  {
    $self->{_inbuffer}.=$data;
    return 1 if(length ($self->{_inbuffer})>=$self->{_file}->{_size})
  }
  else
  {
    my $outdata=$data;
    #utf8::decode($outdata);
    $self->{_inbuffer}.=$data;
      #print "DATA:<".$data.">\n";
    while($self->{_inbuffer}=~s/(\[\{.*?\}\]\n)//s)
    {
         #print "\nINBUFFER='".$self->{_inbuffer}."'\n";
      #my $t=$1;
      push @{$self->{_messages}}, $1."\n";
    }
      #print "END (messages ".scalar(@{$self->{_messages}}).")\n";
      $self->{_inbuffer}=~s/\n//;
    if(length $self->{_inbuffer}>$self->{_maxsize})
    {
      $self->{_inbuffer}="";
      $self->send("[{\"error\":{\"text\":\"Message too long\"}}]\n");
      $self->close();
    }
    return 1 if(scalar(@{$self->{_messages}})>0);
  }
  return 0;
}

sub pop_buffer
{
  my $self=shift;
  $self->{_startidletime}=time;
  if($self->binary)
  {
    my $buf=$self->{_inbuffer};
    $self->{_inbuffer}="";
    return $buf;
  }
  #$self->{_inbuffer}="";
  my $data=shift @{$self->{_messages}};
  my $message=$data;  
  
  if(defined $message)
  {
    utf8::decode($message) if  not utf8::is_utf8($message);
    print BRIGHT_BLACK, $self->{_sport};
    print BRIGHT_GREEN,"-->",RESET;
    print BRIGHT_BLACK,$message,RESET;
    
    #print $self->{_sport};
    #print "-->",RESET;
    #print $message,RESET;
  }
  return $data;
}


sub send
{
  my $self=shift;
  my $message=shift;
  my $status=shift;
  my $sb=0;
  my $outmessage;
  #utf8::encode($message) if  utf8::is_utf8($message);
  #$self->{'_ss'}->{'waitforresponse'}=time;
  if(defined $status)
  {
   $self->add_status($status);
  }
  else
  {
   $self->add_status('waitforresponse');
  }
  if($self->{_socket}->connected)
  {
    #print "socket is connected\n";
    my $outmessage=$self->{_outbuffer}.$message;
    my $original_message=$outmessage;
    my $message_length=length($outmessage); # Just for debug
    if($self->binary)
    {
      #Encode::_utf8_off($outmessage);
      print "sending $message_length bytes\n";
      my $sb=0;
      my $bytes_sent=0;
      while($sb=$self->{_socket}->send($outmessage))
      {
        $bytes_sent+=$sb;
        substr($outmessage,0,$sb)="";
        last if($bytes_sent>=$message_length);
        print "\n$sb/$message_length\n";
      }
      
      if(length $outmessage>0)
      {
        $self->{_outbuffer}=$outmessage;
        $self->{_waitforsend}=1;
      }
      else
      {
        $self->{_outbuffer}="";
        $self->{_waitforsend}=0;
        $self->close();
      }
    }
    else
    {
      utf8::encode($outmessage) if  utf8::is_utf8($outmessage);
      $sb=$self->{_socket}->send($outmessage)//0;
      my $bytes_send=$sb; # just for debug
      while($sb < length ($outmessage))
      {
        substr($outmessage,0,$sb)="";
        $sb=$self->{_socket}->send($outmessage)//0;
        $bytes_send+=$sb;
      }
      utf8::decode($original_message) if (not utf8::is_utf8($original_message));
      print BRIGHT_BLACK, $self->{_sport};
      print BRIGHT_YELLOW,"<--",RESET;
      print BRIGHT_BLACK, "($bytes_send/".$message_length.")$original_message",RESET;
      $self->{_outbuffer}='';
      $self->{_waitforsend}=0;
    }
  }
  else
  {
    #print "Socket is not connected, waiting\n";
    $self->{_outbuffer}.=$message;
    $self->{_waitforsend}=1;
  }
  $self->{_startidletime}=time;
}
#======================END OF BUFFER ROUTINES=================================

sub ping
{
  my $self=shift;
  #if($self->{_status} eq 'idle' or $self->{_status} eq 'waitforresponse')
  {
    my @chars = ("A".."Z","a".."z",0 .. 9);
    my $id = join("", @chars[ map { rand @chars } (0..7) ]);
    my $m="[{\"ping\":{\"id\":\"$id\"}}]\n";
    $self->{_pingid}=$id;
    $self->{_ping_failed}++;
    $self->send($m,'waitforping');
  }
}

sub pong
{
  my $self=shift;
  my $id=shift;
  if($self->{_pingid} eq $id)
  {
    $self->{_ping_failed}=0;
    $self->{_startidletime}=time;
    $self->del_status('waitforping');
  }
}

sub DESTROY
{
  warn "client destroyed!\n";
}

return 1;
