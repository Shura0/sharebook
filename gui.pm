
package Gui;

use v5.10;
use utf8;
use IO::Socket;
use IO::Select;
use Data::Dumper;
use Exception::Class::TryCatch;
use JSON;

# TODO: remove global variables

my $SPORT;
my $SELECT;
my $MAX_CONNECTIONS=1;
my %CLIENTS;
sub start_server
{
  my $SPORT=shift;
  $PORT=$SPORT if ($SPORT);
  print "GUI port is $PORT\n";
  $SOCKET = new IO::Socket::INET (
  LocalHost => 'localhost',
  LocalPort => $SPORT,
  Proto => 'tcp',
  Listen => 32768,
  Reuse => 1,
  timeout => 5);
  die "Could not create gui socket: $!n" unless $SOCKET;
  print "gui socket created\n";
  $SELECT = new IO::Select($SOCKET); # create handle set for reading
  #binmode $SOCKET, ":utf8";
  $MAX_CONNECTIONS=1;
   %CLIENTS=();
}

sub send
{
  my $text=shift;
  foreach my $i (keys %CLIENTS)
  {
    if($CLIENTS{$i}->socket() and $CLIENTS{$i}->socket()->connected())
    {
      $CLIENTS{$i}->send($text) if $CLIENTS{$i};
    }
  }
}

sub get_messages
{
($SOCKET and $SELECT)or return;
  my @messages;
  while(my @ready = $SELECT->can_read(0.1)) {
    foreach my $rh (@ready) {
      #additional peer processing;
      if ($rh == $SOCKET) {
        my $cl=$SOCKET->accept;
        #binmode $cl, "utf8";
        $SELECT->add($cl);
        my ($port, $iaddr) =sockaddr_in(getpeername($cl));
        $iaddr = inet_ntoa($iaddr);
        my $client=Peer->new;
        $client->socket($cl);
        $client->ip($iaddr);
        $client->port($port);
        $client->type('GUI');
        #$client->add_status('waitforresponse');
        $CLIENTS{$cl}=$client;
        print "new GUI client!\n";
        undef $client;
      }
      else {
        my $buf;
        #utf8::decode($buf);
        my $message;
        if(!sysread($rh, $buf, 1024))
        {
          print "GUI client has closed connection\n";
          $SELECT->remove($rh);
          close($rh);
          delete $CLIENTS{$rh};
          next;
        }
        if($CLIENTS{$rh}->add_buffer($buf))
        {
          while ($message=$CLIENTS{$rh}->pop_buffer)
          {
          eval {
            my $json=new JSON;
            $json=$json->utf8(1);
            @obj= $json->decode($message);
            };
            if(!catch my $e)
            {
              my $o=$obj[0][0];
              push @messages, $o;
            }
            else{
              warn "MAILFORMED PACKET from GUI! $!\n";
            }
          }
        }
      }
    }
  }
  return @messages;
}


1;
