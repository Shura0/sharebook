#net_routine

package Message;
use Data::Dumper;
use v5.10;
use utf8;

#$ENV{PERL_UNICODE}=6;

sub new
{
  # TODO: swap $data and $text
  my $class=shift;
  my $sip=shift or return 0;
  my $sport=shift or return 0;
  my $ssocket=shift or return 0;
  my $text=shift or return 0;
  my $data=shift;
  my $self={};
  $sip and $self->{_sip}=$sip;
  $sport and $self->{_sport}=$sport;
  $ssocket and $self->{_socket}=$ssocket;
  $text and $self->{_text}=$text;
  $data and $self->{_data}=$data;
  bless $self, $class;
  return $self;
}
sub text
{
  my $self=shift;
  return $self->{_text};
}
sub data
{
  my $self=shift;
  return $self->{_data};
}
  sub ip
  {
    my $self=shift;
    return $self->{_sip};
  }
  sub port
  {
    my $self=shift;
    return $self->{_sport};
  }
  sub socket
  {
    my $self=shift;
    return $self->{_socket};
  }
1;

package Network;
# TODO: Rewrite file transfer mechanism. I don't understand how it works

use utf8;
use v5.10;
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
binmode STDIN, ":utf8";
#$ENV{PERL_UNICODE}="io";

use IO::Socket;
use IO::Select;
use strict;
use JSON;
use Data::Dumper;
use Term::ANSIColor;
use Term::ANSIColor qw(:constants);
use Exception::Class::TryCatch;
my $PORT=9222;
my $MYIP;
my $BANDWIDTH;
my $MODE='master';
my $CANBEMASTER=0;
my $MAX_CONNECTIONS=50;
my $NAT=1;
my $SELECT;
my $CHILDSELECT;
my %CLIENTS;

# TODO: Add $GETS and $MY_GETS cleaning;
my %GETS;
my %MY_GETS;
#==
my %SEARCHES;
my %MY_SEARCHES;
my $SEARCH_KEEP_TIME=120; # in seconds
my $SEARCH_TIMEOUT=5; # in seconds

# TODO: Do something with %NH
my %NH;
my @CLIENTS_TO_REJECT;
my $SERVER_SOCKET;
my $LAST_PEER_PROCESSING_TIME=0;
my $LAST_RECONNECT_PROCESSING_TIME=0;
my $LOST_CONNECTION=0;
my $HOST="";
my $ID=""; # ID for connects


$|=1;

sub get_clients_number
{
  return scalar(keys %CLIENTS);
}

sub make_search
{
  my $hash=shift;
  my $title=shift;
  my $first_name=shift;
  my $last_name=shift;
  my $lang=shift;
  my $filename=shift;
  my $id=shift//generate_id();
  my $request={
    id=>$id,
    ttl=>10,
    hash=>$hash,
    title=>$title,
    author=>{
      first_name=>$first_name,
      last_name=>$last_name
      },
    lang=>$lang,
    filename=>$filename
    };
  my $json = new JSON;
  $json=$json->utf8(1);
  my $text= $json->encode( $request );
  $text="[{\"search\":".$text."}]\n";
  #print "SEARCH REQUEST = $text";
  #print Dumper $request;
  foreach my $i (keys %CLIENTS)
  {
    $CLIENTS{$i}->send($text,"idle");
  }
  $MY_SEARCHES{$id}=time;
  return $id;
}

sub search_result
{
  my $id=shift;
  $id or return 0;
  my $books=shift;
  $books or return 0;
  my $answer={
    id=>$id,
    books=>$books
  };
  if(defined $SEARCHES{$id} and defined $CLIENTS{$SEARCHES{$id}->{'socket'}})
  {
    my $json = new JSON;
    $json=$json->utf8(1);
    my $text= $json->encode( $answer );
    $text="[{\"search_result\":".$text."}]\n";
    #print "search_result: $text\n";
    $CLIENTS{$SEARCHES{$id}->{'socket'}}->send($text,"idle");
  }

}

sub merge_results # merge search result
{

}


# TODO: Move to other file
sub str_to_ipandport
{
  my $string=shift;
  $string or return undef;
  #print GREEN, $string."\n", RESET;
  if($string=~/(^[^:]+):([\d]+)/)
  {
    my $ip=$1;
    my $port=$2;
   # print GREEN, "($ip) ($port)\n",RESET;
    return ($ip, $port);
  }
  return undef;
}

sub generate_id
{
  my @chars = ("A".."Z","a".."z",0 .. 9);
  return join("", @chars[ map { rand @chars } (0..7) ]);
}

sub who_requested_file
{
  my $hash=shift;
  foreach my $i (keys %GETS)
  {
    if($GETS{$i} eq $hash)
    {
      return $i;
    }
  }
  return "me";
}

sub get_file
{
  my $m=shift;
  #TODO: rewrite
  my $hash=$m->{'hash'} or return;
  my $owner_ip=$m->{'owner_ip'} or return;
  my $owner_port=$m->{'owner_port'} or return;
  my $master_ip=$m->{'master_ip'};
  my $master_port=$m->{'master_port'};
  my $transferto=$m->{'transfer_to'};
  print "Added GET hash. Host: $owner_ip:$owner_port, hash: $hash\n";
  my $client;
  if($master_ip and $master_port)
  {
    print "getting file. Trying to connect with master $master_ip:$master_port\n";
    $client=_connect($master_ip, $master_port);
    return if(! $client);
    $client->type('get');
    $client->send("[{\"connect\":{\"get\":true}}]\n",'waitforconnect');
    $client->fileowner("$owner_ip:$owner_port");
    #$client->filetransferto($transferto);
    $client->filehash($hash);
    print "Added GET hash. Host: $master_ip:$master_port, hash: $hash\n";
    $MY_GETS{"$master_ip:$master_port"}=$hash;
  }
  else
  {
    $MY_GETS{"$owner_ip:$owner_port"}=$hash;
    print "no master is assigned, trying to connect to owner\n";
    my $client=_connect($owner_ip,$owner_port);
    return if(! $client);
    $client->type('get');
    $client->send("[{\"connect\":{\"get\":true}}]\n",'waitforconnect');
    #$client->fileowner("$owner_ip:$owner_port");
    #$client->filetransferto($transferto);
    $client->filehash($hash);
  }
}

sub send_file
{
  my $socket=shift or return 0;
  my $hash=shift or return 0;
  my $size=shift;
  my $filename=shift;
  my $data=shift;
  my $s;
  if(ref($socket) and ref($socket)=~/Socket/i)
  {
    $s=$socket;
    return 0 if(! defined $CLIENTS{$s});
  }
  else{
    my ($ip, $port)=str_to_ipandport($socket);
    if($ip and $port)
    {
      print "Looking for requester...\n";
      foreach my $i (keys %CLIENTS)
      {
        print "host=".$CLIENTS{$i}->ip().":".$CLIENTS{$i}->sport()."...";
        if($CLIENTS{$i}->ip eq $ip and $CLIENTS{$i}->sport eq $port and $CLIENTS{$i}->type eq 'get')
        {
          $s=$i;
          print "found!\n";
          delete $GETS{"$ip:$port"};
          last;
        }
        print "\n";
      }
    }
  }
  if(!($s and $CLIENTS{$s} and $CLIENTS{$s}->type eq 'get'))
  {
    #print $s."\n";
    #print Dumper $CLIENTS{$s};
    print "Client not found!\n";
    return 0;
  }
  if(!($size and $filename and $data))
  {
    print "I have no file data!\n";
    $CLIENTS{$s}->send("[{\"get_result\":{\"hash\":\"$hash\", \"notfound\":true}}]\n");
    $CLIENTS{$s}->close();
    return 0;
  }
    #my $size= -s $filename;
  #utf8::encode($filename);
  if(utf8::is_utf8($filename))
  {
    print "UTF8 string!\n";
  }
  else
  {
    print "NON UFT8 string!\n";
  }
  $CLIENTS{$s}->filename($filename);
  $CLIENTS{$s}->filehash($hash);
  $CLIENTS{$s}->filesize($size);
  $CLIENTS{$s}->filedata($data);
  $CLIENTS{$s}->send("[{\"get_result\":{\"hash\":\"$hash\",\"name\":\"$filename\",\"size\":$size}}]\n");
  return 1;
}

sub _send_file
{
  my $socket=shift;
  return if (!defined $CLIENTS{$socket});
  if(!defined $CLIENTS{$socket}->filedata())
  {
    print "No data for client!\n";
    $CLIENTS{$socket}->close();
    return;
  }
  $CLIENTS{$socket}->binary(1);
  $CLIENTS{$socket}->del_status('waitforresponse');
  $CLIENTS{$socket}->send($CLIENTS{$socket}->filedata(),'idle');
  #$CLIENTS{$socket}->close();

}

sub start_server
{
  my $SPORT=shift;
  my $MASTER=shift;
  $MAX_CONNECTIONS=shift;
  $MODE='client';
  $MODE='master' if $MASTER;
  $PORT=$SPORT if ($SPORT);
  print "source port is $PORT\n";
  $SERVER_SOCKET = new IO::Socket::INET (
  #LocalHost => 'localhost',
  LocalPort => $SPORT,
  Proto => 'tcp',
  Listen => 32768,
  Reuse => 1,
  timeout => 5);
  die "Could not create socket: $!n" unless $SERVER_SOCKET;
  print "socket created\n";
  #binmode $SERVER_SOCKET, ":utf8";
  $SELECT = new IO::Select($SERVER_SOCKET); # create handle set for reading
  $CHILDSELECT = new IO::Select(); # create handle for reading info from the children
  #$MAX_CONNECTIONS=5;
  $SEARCH_KEEP_TIME=120; # in seconds
  $NAT=1;
  $ID=generate_id();
}

sub check_nat{ #host, port
  my $host=shift;
  my $port=shift;
  my $client=_connect($host, $port);
  $client->send("[{\"connect\":{\"checknat\":1,\"your_ip\":\"$host\"}}]\n","waitfornat");
    $client->type("nat");
    print "checking $host for nat...\n";
}

sub _connect
{
  my $host=shift;
  my $port=shift;
  print "trying to connect to $host:$port\n";
  my $sock=new IO::Socket::INET (
  PeerAddr => $host,
  PeerPort => $port,
  Proto => 'tcp',
  Blocking => 0);
  if(!$sock)
  {
    print "Could not create socket: $!n";
    return undef;
  }
  $SELECT->add($sock);
  #binmode $sock, ":utf8";
  my $client=Peer->new;
  $client->socket($sock);
  $client->ip($host);
  $client->port($port);
  $client->sport($port);
  $CLIENTS{$sock}=$client;
  return $CLIENTS{$sock};
}

sub connect
{
  my $in=shift or return;
  my $host=$in->{'host'};
  my $port=$in->{'port'};
  $BANDWIDTH=$in->{'bandwidth'}//256;
  my $type=$in->{'type'}//"connect";
  $CANBEMASTER=$in->{'canbemaster'}//0;
  my $client=_connect($host,$port);
  return if(!$client);
  if($type eq 'connect')# connect to master
  {
    $client->type('master');
    my $mm='false';
    $mm='true' if $MODE eq 'master';
    $HOST="$host:$port"; # add host to host list
    #$mm='false' if $MODE eq 'client';
    
    $client->send("[{\"connect\":{\"canbemaster\": \"$CANBEMASTER\",".
                  "\"version\":1,\"port\":$PORT,".
                  "\"bandwidth\":$BANDWIDTH,\"id\":\"$ID\"}}]\n",'idle');
  }
  $MODE='client';
}

sub reconnect
{
  if($HOST)
  {
    print "reconnect to $HOST\n";
    my ($ip, $port)=str_to_ipandport($HOST);
    Network::connect({
                      host=>$ip,
                      port=>$port,
                      bandwidth=>$BANDWIDTH});
  }
  $LAST_RECONNECT_PROCESSING_TIME=time();
}

sub update_neigborhoods
{
  #my $new_host=shift;
  my $nh="";
  #my ($ip, $port)=str_to_ipandport($new_host);
  foreach my $i (keys %CLIENTS)
  {
    my $u=$CLIENTS{$i};
    if($u->type eq 'master' and $u->nat eq 'false')
    {
      $nh.="{\"ip\":\"".$u->ip()."\",\"port\":\"".$u->port()."\"},";
    }
  }
  $nh=~s/,$//;
  print "NH=$nh";
  $nh="{}" if $nh eq "";
  print "Sending update to clients\n";
  foreach my $i (keys %CLIENTS)
  {
    my $u=$CLIENTS{$i};
    next if($u->type eq 'nat');
    #next if ($u->ip() eq $ip and $u->port() == $port);
    $u->send("[{\"update\":{\"neigborhoods\":[$nh]}}]\n","idle");
  }
}

sub peers_processing
{
  # TODO: Add update sending when some clients disconnects
  my $losthost_flag=1;
  my ($ip,$port)=(0,0);
  ($ip,$port)=str_to_ipandport($HOST) if $HOST;
  foreach my $i (keys %CLIENTS)
  {
    $CLIENTS{$i}->process;
    if($MODE eq 'master')
    {
      if($CLIENTS{$i}->idletime>10 and (! $CLIENTS{$i}->binary()))
      {
        $CLIENTS{$i}->ping;
      }
    }
    if($HOST and $ip eq $CLIENTS{$i}->ip() and $port == $CLIENTS{$i}->port())
    {
      $losthost_flag=0;
    }
    if($CLIENTS{$i}->is_closing)
    {
      my $ip=$CLIENTS{$i}->ip;
      my $port=$CLIENTS{$i}->port;
      delete $NH{"$ip:$port"};
      $SELECT->remove($CLIENTS{$i}->socket);
      close($CLIENTS{$i}->socket);
      delete $CLIENTS{$i};
      if(get_clients_number()<1){print "no more clients\n";}
    }
  }
  $LOST_CONNECTION=$losthost_flag if $HOST;
  if(get_clients_number()==0 and $MODE eq 'client')
  {
    print "I'm lost the master!\nLooking for neigborhoods...";
    my $flag=0;
    foreach my $i (keys %NH)
    {
      my ($ip, $port)=str_to_ipandport($i);
      if(Network::connect({host=>$ip, port=>$port}))
      {
        $flag=1;
        delete $NH{"$ip:$port"};
        last;
      }
    }
    if(!$flag)
    {
      print "Not found\n";
      # TODO: Change this. If node was a master then it should be a master after reconnect and should not if was not.
      $MODE='master';
      print "I don't see master anymore, going to be a master\n";
    }
  }
  # clear $SEARCHES
  for my $i (keys %SEARCHES)
  {
    #print "clear searches:".$SEARCHES{$i}->{'time'}."\n";
    if($SEARCHES{$i}->{'time'} and $SEARCHES{$i}->{'time'}+$SEARCH_KEEP_TIME<time())
    {
      delete $SEARCHES{$i};
    }
  }
  # clear $MY_SEARCHES
  for my $i (keys %MY_SEARCHES)
  {
    if($MY_SEARCHES{$i} and $MY_SEARCHES{$i}+$SEARCH_KEEP_TIME+60<time()) # keep mysearches $SEARCH_KEEP_TIME + 60 seconds
    {
      delete $MY_SEARCHES{$i};
    }
  }

  $LAST_PEER_PROCESSING_TIME=time();
  #print "My IP is $MYIP\n";
}

#====================
#===== GUI routines =
#====================

sub get_peers # retrns list of connected hosts (for GUI)
{
  my $json=new JSON;
  $json=$json->utf8(1);
  my $text="";
  foreach my $i (keys %CLIENTS)
  {
    if($CLIENTS{$i}->type() eq 'client' or $CLIENTS{$i}->type() eq 'master')
    {
      my $o={
      type=>$CLIENTS{$i}->type(),
      host=>{
        ip=>$CLIENTS{$i}->ip,
        port=>$CLIENTS{$i}->port,
        nat=>$CLIENTS{$i}->nat,
        bandwidth=>$CLIENTS{$i}->bandwidth
        }
      };
      $text.=$json->encode($o).",";
    }
  }
  $text=~s/,$//;
  $text="{}" if($text eq"");
  return $text;
}

sub create_event # for GUI
{
  my $peer=shift;
  my $event=shift;
  my $o={
    event=>'connect',
    status=>$event,
    peer=>{
        ip=>$peer->ip,
        port=>$peer->port,
        nat=>$peer->nat,
        type=>$peer->type,
        canbemaster=>$peer->canbemaster
        }
    };
    my $json=new JSON;
    $json=$json->utf8(1);
    my $text=$json->encode($o);
    return Message->new($peer->ip,$peer->port,$peer->socket,$o,$text);
}
sub create_custom_event # for GUI
{
  my $peer=shift;
  my $event=shift;
  my $json=new JSON;
  $json=$json->utf8(1);
  my $text=$json->encode($event);
  return Message->new($peer->ip,$peer->port,$peer->socket,$event,$text);
}


# ==========================NETWORK PROCESSING ================================

sub _process_client_mode
{
  my $o=shift;
  my $rh=shift;
  my $message=shift;
  my @messages;
  if($o->{'connect'})
  {
    if($o->{'connect'}->{'id'} and $o->{'connect'}->{'id'} eq $ID)
      {
        print "It's mine connection! Abort\n";
        $CLIENTS{$rh}->close();
        return @messages;
      }
    if($o->{'connect'}->{'checknat'})
    {
      print "there is no nat\n";
      $NAT=0;
      $MYIP=$o->{'connect'}->{'your_ip'};
      $CLIENTS{$rh}->send("[{\"accepted\":{\"nat\":false,\"my_ip\":\"$MYIP\",\"port\":\"$PORT\"}}]\n", "idle");
    }
    else
    {
      if($o->{'connect'}->{'get'})
      {
        $CLIENTS{$rh}->type('get');
        $CLIENTS{$rh}->send("[{\"accepted\":{\"get\":true}}]\n");
      }
      else
      {
        foreach my $t (keys %CLIENTS)
        {
          if($CLIENTS{$t}->type eq 'master')
          {
            my $ip=$CLIENTS{$t}->ip;
            my $port=$CLIENTS{$t}->port;
            $CLIENTS{$rh}->send("[{\"rejected\":{\"master\":{\"ip\":\"$ip\",\"port\":\"$port\"}}}]\n");
            $CLIENTS{$rh}->close();
            last;
          }
        }
      }
    }
  }
  elsif($o->{'rejected'}) #client
  {
    if($o->{'rejected'}->{'master'})
    {
      if(! ($o->{'rejected'}->{'master'}->{'ip'} eq $MYIP and $o->{'rejected'}->{'master'}->{'port'} == $PORT))
      {
        Network::connect({host=>$o->{'rejected'}->{'master'}->{'ip'}, port=>$o->{'rejected'}->{'master'}->{'port'}});
      }
    }
    if($o->{'rejected'}->{'error'})
    {
      if($o->{'rejected'}->{'error'}->{'text'} eq 'wrong id')
      {
        print "Seems like I've connected to myself\n";
        my $ip=$CLIENTS{$rh}->ip();
        my $port=$CLIENTS{$rh}->port();
        if($HOST and $HOST eq "$ip:$port")
        {
          $HOST="";
        }
      }
    }
  }
  elsif($o->{'update'}) #client
  {
    if($o->{'update'}->{'master'} and ($o->{'update'}->{'master'} eq 'you') and $CANBEMASTER)
    {
      $MODE='master';
      %NH=();
      $CLIENTS{$rh}->send("[{\"accepted\":{\"master\":\"ok\"}}]\n","idle");
      print "I AM MASTER NOW!\n";
    }
    elsif($o->{'update'}->{'neigborhoods'} and $CLIENTS{$rh}->type() eq 'master')
    {
      my $nh=$o->{'update'}->{'neigborhoods'};
      for(my $i=0;$i<@{$nh};$i++)
      {
        print "ref nh[$i]=".ref ${$nh}[$i]."\n";
        my $str=${$nh}[$i]->{'ip'}.":".${$nh}[$i]->{'port'};
        $NH{$str}=1;
      }
      print "Neigborhoods: ".keys(%NH)."\n";
    }
  }
  elsif($o->{'search'} && $CLIENTS{$rh}->type() eq 'master') #client
  {
    print "SEARCH..";
    if($o->{'search'}->{'id'})
    {
      print "ID=".$o->{'search'}->{'id'}."\n";
      if(! defined $SEARCHES{$o->{'search'}->{'id'}})
      {
        $SEARCHES{$o->{'search'}->{'id'}}={
                                time=>time,
                                socket=>$CLIENTS{$rh}->socket()
                                };
        my $m=Message->new($CLIENTS{$rh}->ip,$CLIENTS{$rh}->port,$CLIENTS{$rh}->socket,$o);
        push @messages, $m;
      }
    }
  }
  elsif($o->{'search_result'} and defined $MY_SEARCHES{$o->{'search_result'}->{'id'}})
  {
    
    my $m;
    my $ip=$CLIENTS{$rh}->ip;
    my $port=$CLIENTS{$rh}->port;
    my $nat=$CLIENTS{$rh}->nat;
    $port=$CLIENTS{$rh}->sport() if $nat ne 'false';
    my $flag=0;
    if(!$o->{'search_result'}->{'owner'}) # no owner's information
    {
       $o->{'search_result'}->{'owner'}={
                                        ip=>$ip,
                                        port=>$port,
                                        nat=>'false' # it's my master, he can't has NAT!
                                        };
      $flag=1;
    }
    if(!$o->{'search_result'}->{'master'}) # no master's information
    {
      $o->{'search_result'}->{'master'}={
                                        ip=>$ip,
                                        port=>$port,
                                        nat=>'false' # it's my master, he can't has NAT!
                                        };
      $flag=1;
    }
    if($flag)
    {
       my $json=new JSON;
       $json=$json->utf8(1);
       my $text= "[".$json->encode($o)."]\n";
       $m=Message->new($CLIENTS{$rh}->ip,$CLIENTS{$rh}->port,$CLIENTS{$rh}->socket,$o,$text);
    }
    else
    {
      $m=Message->new($CLIENTS{$rh}->ip,$CLIENTS{$rh}->port,$CLIENTS{$rh}->socket,$o,$message);
    }
    push @messages, $m;
  }
  return @messages;
}

sub _process_server_mode
{
  my $o=shift;
  my $rh=shift;
  my $message=shift;
  my @messages;
  if($o->{'connect'})
  {
    if($o->{'connect'}->{'id'} and $o->{'connect'}->{'id'} eq $ID)
    {
      print "It's mine connection! Abort\n";
      $CLIENTS{$rh}->send("[{\"rejected\":{\"error\":{\"text\":\"wrong id\"}}}]\n");
      $CLIENTS{$rh}->close();
      return @messages;
    }
    if($o->{'connect'}->{'checknat'})
    {
      print "there is no nat\n";
      $NAT=0;
      $MYIP=($o->{'connect'}->{'your_ip'} or "");
      $CLIENTS{$rh}->send("[{\"accepted\":{\"nat\":false,\"my_ip\":\"$MYIP\",\"port\":\"$PORT\"}}]\n");
      $CLIENTS{$rh}->close();
    }
    elsif($o->{'connect'}->{'get'})
    {
      $CLIENTS{$rh}->type('get');
      $CLIENTS{$rh}->send("[{\"accepted\":{\"get\":true}}]\n");
    }
    elsif($o->{'connect'}->{'put'})
    {
      $CLIENTS{$rh}->type('get');
      if($o->{'connect'}->{'hash'})
      {
        my $hash=$o->{'connect'}->{'hash'};
        my $flag=0;
        print "PUT request, hash=$hash. Looking in my base:\n";
        foreach my $k (keys %GETS)
        {
          print "   $GETS{$k}\n";
          if($GETS{$k} eq $hash)
          {
            $flag=1;
          }
        }
        if($flag)# we are waiting for this file
        {
          $CLIENTS{$rh}->send("[{\"get\":{\"hash\":\"$hash\"}}]\n");
        }
      }
    }
    else{
      print "CONNECT from $o->{'connect'}->{'port'}\n";
      $CLIENTS{$rh}->bandwidth($o->{'connect'}->{'bandwidth'});
      $CLIENTS{$rh}->port($o->{'connect'}->{'port'});
      $CLIENTS{$rh}->version($o->{'connect'}->{'version'});
      $CLIENTS{$rh}->canbemaster($o->{'connect'}->{'canbemaster'});
      $CLIENTS{$rh}->del_status('waitforresponse');
      #Too many clients
      print "CLIENTS: ".get_clients_number()."/$MAX_CONNECTIONS\n";
      if(get_clients_number()>$MAX_CONNECTIONS) #We need a new master!
      {
        my ($ip, $port, $nh);
        my $flag=0;
        print "trying to fing free master...";
        foreach my $i (keys %CLIENTS)
        {
          if($CLIENTS{$i}->type eq "master")
          {
            # send warning to masters
            $CLIENTS{$i}->send("[{\"update\":{\"full\":\"".get_clients_number()."/$MAX_CONNECTIONS\"}}]\n","idle");
            my $util=$CLIENTS{$i}->utilization();
            print "UTIL: $util\n";
            if($CLIENTS{$i}->utilization()=~/(\d+)\/(\d+)/)
            {
              print "utilization $1/$2\n";
              if(($1 and $2))
              {
                next if ($1/$2>0.7);
              }
            }
            $ip=$CLIENTS{$i}->ip;
            $port=$CLIENTS{$i}->port;
            $flag=1;
            $nh="{\"ip\":\"$ip\",\"port\":\"$port\"}";
          }
        }
        if($flag)
        {
          print "found!\n";
          $CLIENTS{$rh}->send("[{\"rejected\":{\"master\":$nh}}]\n");
          $CLIENTS{$rh}->close();
        }
        else # if we have not master peers we need new master!
        {
          # temporary accept connection
          $CLIENTS{$rh}->send("[{\"accepted\":{\"ok\":true,\"neigborhoods\":[]}}]\n","idle");
          print "not found\nNew master election\n";
          my $mb=0;
          my $candidate=0;
          foreach my $i (keys %CLIENTS)
          {
            next if($CLIENTS{$i}->type eq 'nat');
            if(($CLIENTS{$i}->bandwidth() > $mb) and
                ($CLIENTS{$i}->nat eq 'false') and
                ($CLIENTS{$i}->type eq 'client') and
                ($CLIENTS{$i}->canbemaster() eq 'true'))
            {
              $mb=$CLIENTS{$i}->bandwidth;
              $candidate=$CLIENTS{$i};
            }
          }
          if($candidate)
          {
            my $ip=$candidate->ip;
            my $port=$candidate->port;
            print "got it: $ip:$port\n";
            $candidate->send("[{\"update\":{\"master\":\"you\",\"neigborhoods\":[]}}]\n",'waitformaster');
            if($candidate != $CLIENTS{$rh})
            {
              push @CLIENTS_TO_REJECT,$CLIENTS{$rh};
            }
          }
        }
      }
      else{# Accept connection
        my @n;
        my ($ip, $port);
        foreach my $i (keys %CLIENTS)
        {
          my $o=$CLIENTS{$i};
          if($o->type() eq "master" and $o->nat() eq "false")
          {
            print "Found a master! ";
            $ip=$o->ip;
            $port=$o->port;
            push @n, "$ip:$port";
            print "$ip : $port\n";
          }
        }
        my $nh="";
        foreach (@n)
        {
          if(/(^[^:]+)/)
          {
            $ip=$1;
          }
          if(/:(\d+)/)
          {
            $port=$1;
          }
          $nh.="{\"ip\":\"$ip\",\"port\":\"$port\"},";
        }
        $nh=~s/,$//;
        print "NH=$nh\n";
        $CLIENTS{$rh}->send("[{\"accepted\":{\"ok\":true,\"neigborhoods\":[$nh]}}]\n","idle");
        check_nat($CLIENTS{$rh}->ip, $o->{'connect'}->{'port'});
        push @messages,create_event($CLIENTS{$rh},"connected");
      }
    }
  }
  elsif($o->{'rejected'})
  {
    if($o->{'rejected'}->{'error'})
    {
      if($o->{'rejected'}->{'error'}->{'text'} eq 'wrong id')
      {
        print "Seems like I've connected to myself\n";
        my $ip=$CLIENTS{$rh}->ip();
        my $port=$CLIENTS{$rh}->port();
        if($HOST and $HOST eq "$ip:$port")
        {
          $HOST="";
        }
      }
    }
  }
  elsif($o->{'update'})#master mode
  {
    print ("update from master #1!\n");
    if($CLIENTS{$rh}->type eq 'master')
    {
      my $f;
      if($f=$o->{'update'}->{'full'})
      {
        print ("update from master! #2\n$f\n");
        $CLIENTS{$rh}->utilization($f);
      }
    }
    return 0;
  }
  elsif($o->{'search'}) #master mode
  {
    print "SEARCH..";
    if($CLIENTS{$rh}->searchtime()+$SEARCH_TIMEOUT > time)
    {
      print "Search too often. Ignore\n";
      return @messages;
    }
    $CLIENTS{$rh}->searchtime(time);
    if($o->{'search'}->{'id'})
    {
      print "ID=".$o->{'search'}->{'id'}."\n";
      if(! defined $SEARCHES{$o->{'search'}->{'id'}})
      {
        $SEARCHES{$o->{'search'}->{'id'}}={
                                time=>time,
                                socket=>$CLIENTS{$rh}->socket()
                                };
        if(defined $o->{'search'}->{'ttl'} and ($o->{'search'}->{'ttl'}--)>0)
        {
          $o->{'search'}->{'ttl'}=32 if($o->{'search'}->{'ttl'}>32);
          my $json=new JSON;
          $json=$json->utf8(1);
          my $text= "[".$json->encode($o)."]\n";
          print "\n\n=======SEARCH REQUEST=======\n\n";
          #print $text;
          foreach my $i (keys %CLIENTS)
          {
            next if($CLIENTS{$i}->type() eq "get");
            next if($CLIENTS{$i}->type() eq "nat");
            if($CLIENTS{$i} ne $CLIENTS{$rh})
            {
              $CLIENTS{$i}->binary() or $CLIENTS{$i}->send($text,'idle');
            }
          }
        }
        my $m=Message->new($CLIENTS{$rh}->ip,$CLIENTS{$rh}->port,$CLIENTS{$rh}->socket,$o);
        push @messages, $m;
      }
    }
  }
  elsif($o->{'search_result'}) # master mode
  {
    if(defined $MY_SEARCHES{$o->{'search_result'}->{'id'}})
    {
      my $m;
      print "it's my search";
      my $ip=$CLIENTS{$rh}->ip;
      my $port=$CLIENTS{$rh}->port;
      my $nat=$CLIENTS{$rh}->nat;
      $port=$CLIENTS{$rh}->sport() if($nat ne 'false');
      my $flag=0;
      if(!$o->{'search_result'}->{'owner'}) # no owner's information
      {
        $o->{'search_result'}->{'owner'}={
                              ip=>$ip,
                              port=>$port,
                              nat=>$nat
                              };
        $flag=1;
      }
      if($CLIENTS{$rh}->type() eq 'master' and not $o->{'search_result'}->{'master'})
      {
        $o->{'search_result'}->{'master'}={
                              ip=>$ip,
                              port=>$port,
                              nat=>$nat
                              };
        $flag=1;
      }
      if($flag)
      {
        my $json=new JSON;
        $json=$json->utf8(1);
        my $text= "[".$json->encode($o)."]\n";
        $m=Message->new($CLIENTS{$rh}->ip,$CLIENTS{$rh}->port,$CLIENTS{$rh}->socket,$o,$text);
      }
      else
      {
        $m=Message->new($CLIENTS{$rh}->ip,$CLIENTS{$rh}->port,$CLIENTS{$rh}->socket,$o,$message);
      }
      push @messages, $m;
    }
    else
    {
      #merge_results($o->{'search_result'});
      # TODO: Add limitation 10(?) results for every hash.
      if($SEARCHES{$o->{'search_result'}->{'id'}} and
        $SEARCHES{$o->{'search_result'}->{'id'}}->{'socket'} and
        $CLIENTS{$SEARCHES{$o->{'search_result'}->{'id'}}->{'socket'}})
      {
        if($CLIENTS{$rh}->type() eq 'master') # Got message from master
        {
          #my $text=$message;
          my $flag=0;
          my $ip=$CLIENTS{$rh}->ip;
          my $port=$CLIENTS{$rh}->port;
          my $nat=$CLIENTS{$rh}->nat;
          $port=$CLIENTS{$rh}->sport if($nat ne 'false');
          if(!$o->{'search_result'}->{'master'})
          {
            $o->{'search_result'}->{'master'}={
                                ip=>$ip,
                                port=>$port,
                                nat=>$nat
                                };
            $flag=1;
          }
          if(!$o->{'search_result'}->{'owner'})
          {
            $o->{'search_result'}->{'owner'}={
                                ip=>$ip,
                                port=>$port,
                                nat=>$nat
                                };
            $flag=1;
          }
          if($flag)
          {
            my $json=new JSON;
            $json=$json->utf8(1);
            my $text= "[".$json->encode($o)."]\n";
            $CLIENTS{$SEARCHES{$o->{'search_result'}->{'id'}}->{'socket'}}->send($text,"idle");
          }
          else
          {
            $CLIENTS{$SEARCHES{$o->{'search_result'}->{'id'}}->{'socket'}}->send($message,"idle");
          }
        }
        else # got message from client
        {
          my $ip=$CLIENTS{$rh}->ip;
          my $port=$CLIENTS{$rh}->port;
          my $nat=$CLIENTS{$rh}->nat;
          $port=$CLIENTS{$rh}->sport() if($nat ne 'false');
          $o->{'search_result'}->{'owner'}={
                                ip=>$ip,
                                port=>$port,
                                nat=>$nat
                                };
          my $json=new JSON;
          $json=$json->utf8(1);
          my $text= "[".$json->encode($o)."]\n";
          #utf8::decode($text);
          $CLIENTS{$SEARCHES{$o->{'search_result'}->{'id'}}->{'socket'}}->send($text,"idle");
        }
      }
    }
  }
  return @messages;
} # end of _process_server_mode()

sub _process_both_mode
{
  my $o=shift;
  my $rh=shift;
  my $message=shift;
  my @messages;
  if($o->{'ping'})
  {
    $CLIENTS{$rh}->send("[{\"pong\":{\"id\":\"$o->{'ping'}->{'id'}\"}}]\n", "idle");
  }
  elsif($o->{'pong'})
  {
    $CLIENTS{$rh}->pong($o->{'pong'}->{'id'});
  }
  elsif($o->{'get'}) #TODO: Add hard getting methods via masters through NAT
  {
    if($o->{'get'}->{'ok'})
    {
      _send_file($CLIENTS{$rh}->socket);
    }
    elsif($o->{'get'}->{'owner'} and $MODE eq 'master')
    {
      # TODO: add checking for my ip
      my $ip=$o->{'get'}->{'owner'}->{'ip'};
      my $port=$o->{'get'}->{'owner'}->{'port'};
      my $hash=$o->{'get'}->{'hash'};
      my $flag=0;
      foreach my $i (keys %CLIENTS)
      {
        if($CLIENTS{$i}->ip eq $ip and ($CLIENTS{$i}->port eq $port or $CLIENTS{$i}->sport eq $port))
        {
          $flag=$i;last;
        }
      }
      if($flag)
      {
        # TODO: remove status idle in next line. Here should be waitforresponse and it should be
        # removed when we gets PUT request from this client.
        $CLIENTS{$flag}->send("[{\"get\":{\"hash\":\"$hash\"}}]\n", 'idle');
        $CLIENTS{$rh}->del_status('waitforresponse');
        print "file request from ".$CLIENTS{$rh}->ip().":".$CLIENTS{$rh}->sport()."\n";
        $GETS{$CLIENTS{$rh}->ip().":".$CLIENTS{$rh}->sport()}=$hash;
      }
      else{
        $CLIENTS{$rh}->send("[{\"get_result\":{\"hash\":\"$hash\", \"notfound\":true}}]\n");
        $CLIENTS{$rh}->close();
      }
    }
    else{
      print "get. Type=";
      print $CLIENTS{$rh}->type()."\n";
      if($CLIENTS{$rh}->type() ne "get") # master
      {
        my $client=_connect($CLIENTS{$rh}->ip, $CLIENTS{$rh}->port);
        if($client and $o->{'get'}->{'hash'})
        {
          # TODO: Add checking if file is exist
          my $hash=$o->{'get'}->{'hash'};
          $client->type('get');
          $client->send("[{\"connect\":{\"put\":true,\"hash\":\"$hash\"}}]\n");
        }
      }else
      {
        my $m=Message->new($CLIENTS{$rh}->ip,$CLIENTS{$rh}->sport,$CLIENTS{$rh}->socket,$o);
        push @messages, $m;
      }
    }
  }
  elsif($o->{'get_result'}) #client
  {
    if(!$o->{'get_result'}->{'notfound'})
    {
      my $name=$o->{'get_result'}->{'name'};
      my $size=$o->{'get_result'}->{'size'};
      my $hash=$o->{'get_result'}->{'hash'};
      #utf8::encode($name);
      $CLIENTS{$rh}->filesize($size);
      $CLIENTS{$rh}->filename($name);
      $CLIENTS{$rh}->filehash($hash);
      $CLIENTS{$rh}->send("[{\"get\":{\"ok\":true,\"name\":\"$name\",\"size\":\"$size\"}}]\n", "idle");
      $CLIENTS{$rh}->del_status('waitforresponse');
      $CLIENTS{$rh}->binary(1);
    }
    else
    {
      print "file not found\n";
      # Who requested that file?
      if($MODE eq 'master') 
      {
        my $hash=$o->{'get_result'}->{'hash'};
        my $requester=who_requested_file($hash);
        if($requester ne "me")
        {
          my ($ip,$port)=str_to_ipandport($requester);
          foreach my $i (keys %CLIENTS)
          {
            if($CLIENTS{$i}->ip() eq $ip and $CLIENTS{$i}->port() eq $port)
            {
              $CLIENTS{$i}->send("[{\"get_result\":{\"hash\":\"$hash\", \"notfound\":true}}]\n");
              $CLIENTS{$i}->close();
              delete $GETS{$hash};
            }
          }
        }
      }
    }
  }
  elsif($o->{'accepted'})
  {
    if($CLIENTS{$rh}->get_status('waitforconnect'))
    {
      print "Accepted 'waitforconnect'\n";
      $CLIENTS{$rh}->nat('false');
      $CLIENTS{$rh}->del_status('waitforconnect');
      if($CLIENTS{$rh}->type() eq 'get')
      {
        #$CLIENTS{$rh}->del_status('waitforconnect');
        print "GET\n";
        my $ip=$CLIENTS{$rh}->ip;
        my $port=$CLIENTS{$rh}->sport;
        my $hash=$MY_GETS{"$ip:$port"};
        print "ip=$ip, port=$port, hash=$hash\n";
        print "fileowner=".$CLIENTS{$rh}->fileowner()."\n";
        if(defined $hash)
        {
          if($CLIENTS{$rh}->fileowner())
          {
            my ($ip, $port)=str_to_ipandport($CLIENTS{$rh}->fileowner());
            $CLIENTS{$rh}->send("[{\"get\":{\"hash\":\"$hash\", \"owner\":{\"ip\":\"$ip\",\"port\":\"$port\"}}}]\n",'idle');
          }else{
            $CLIENTS{$rh}->send("[{\"get\":{\"hash\":\"$hash\"}}]\n");
          }
        }
      }
    }
    if($CLIENTS{$rh}->get_status('idle'))
    {
      print "Accepted 'idle'\n";
      if($o->{'accepted'}->{'ok'})
      {
        $CLIENTS{$rh}->nat('false');
      }
      if(my $nh=$o->{'accepted'}->{'neigborhoods'})
      {
        #print ref $nh;
        for(my $i=0;$i<@{$nh};$i++)
        {
          print "ref nh[$i]=".ref ${$nh}[$i]."\n";
          my $str=${$nh}[$i]->{'ip'}.":".${$nh}[$i]->{'port'};
          $NH{$str}=1;
          print "STR=$str\n";
        }
      }
      if($HOST)
      {
        my ($ip, $port)=str_to_ipandport($HOST);
        if(($ip eq $CLIENTS{$rh}->ip()) and ($port == $CLIENTS{$rh}->port()))
        {
          $LOST_CONNECTION=0;
        }
      }
    }
    if($CLIENTS{$rh}->get_status('waitformaster'))
    {
      print "Accepted 'waitformaster'\n";
      $CLIENTS{$rh}->del_status('waitformaster');
      if($o->{'accepted'}->{'master'} eq 'ok')
      {
        print "accepted!\n";
        $CLIENTS{$rh}->type('master');
        my $ip=$CLIENTS{$rh}->ip;
        my $port=$CLIENTS{$rh}->port;
        $NH{"$ip:$port"}=1;
        while (my $s=pop @CLIENTS_TO_REJECT)
        {
          $s->send("[{\"rejected\":{\"master\":{\"ip\":\"$ip\",\"port\":\"$port\"}}}]\n");
          $s->close();
        }
        # send negborhoods list to clients
        update_neigborhoods();
      }
    }
    if($CLIENTS{$rh}->get_status('waitfornat'))
    {
      print "Accepted 'waitfornat'\n";
      if($CLIENTS{$rh}->type() eq 'nat' and $o->{'accepted'}->{'my_ip'} and $o->{'accepted'}->{'port'})
      {
        $CLIENTS{$rh}->close();
        my $peer=Peer->new();
        my $ip=$o->{'accepted'}->{'my_ip'};
        my $port=$o->{'accepted'}->{'port'};
        foreach my $t (keys %CLIENTS)
        {
          if($CLIENTS{$t}->ip eq $ip and $CLIENTS{$t}->port eq $port and $CLIENTS{$t}->type() ne 'nat')
          {
            $peer->ip($CLIENTS{$t}->ip());
            $peer->port($CLIENTS{$t}->port());
            $peer->nat('false');
            $peer->socket($CLIENTS{$t}->socket());
            $peer->type($CLIENTS{$t}->type());
            $CLIENTS{$t}->nat('false');
            if($CLIENTS{$t}->canbemaster() eq 'true')
            {
              $CLIENTS{$t}->send("[{\"update\":{\"master\":\"you\",\"neigborhoods\":[]}}]\n",'waitformaster');
            }
            print "Client $ip:$port has not NAT\n";
            push @messages,create_event($peer,'update');
            last;
          }
        }
      }
    }
  }
  return @messages;
}
#===============================================================================
#==========================================  MAIN FUNCTION =====================
#===============================================================================


sub get_messages #from network
{
  # TODO: add several neigborhoods in update.
  ($SERVER_SOCKET and $SELECT)or return sleep 1;
  my @messages;
  peers_processing();
  # trying to reconnect if connection to master is lost
  if(time - $LAST_RECONNECT_PROCESSING_TIME>30) # call every 30 seconds
  {
    reconnect() if $LOST_CONNECTION;
  }
  while(my @ready = $SELECT->can_read(0.3)) { # in seconds. Waiting for messages
    foreach my $rh (@ready) {
      if ($rh == $SERVER_SOCKET) { # New connection
        my $cl=$SERVER_SOCKET->accept;
        #binmode $cl, "utf8";
        $SELECT->add($cl);
        my ($port, $iaddr) =sockaddr_in(getpeername($cl));
        $iaddr = inet_ntoa($iaddr);
        my $client=Peer->new;
        $client->socket($cl);
        $client->ip($iaddr);
        $client->port($port);
        $client->sport($port);
        $client->type('client');
        $client->add_status('waitforresponse');
        $CLIENTS{$cl}=$client;
        print "new client! ".$client->ip.":".$client->sport."\n";
        undef $client;
      }
      else { # New data from existing peer
        my $buf;
        #utf8::decode($buf);
        my $message;
        if(!sysread($rh, $buf, 1024)) # buffer size 1Kb.
        { # Last data from peer
          if($CLIENTS{$rh}->binary())
          {
            print "file transfer is done\n";
            close $CLIENTS{$rh}->filedescriptor();
          }
          else
          {
            push @messages,create_event($CLIENTS{$rh},"disconnected") if $CLIENTS{$rh}->type() ne 'nat';
            if($CLIENTS{$rh}->filehash()) # filedownloading is failed
            {
              print "file transfer failed\n";
              push @messages, create_custom_event($CLIENTS{$rh},{event=>'download_failed',hash=>$CLIENTS{$rh}->filehash()});
            }
          }
          print "Client closed connection\n";
          $SELECT->remove($rh);
          close($rh);
          delete $CLIENTS{$rh};
          print get_clients_number()." clients left\n";
          next;
        }
        if($CLIENTS{$rh}->add_buffer($buf))
        {
          while ($message=$CLIENTS{$rh}->pop_buffer)
          {
            #print "pop buffer!\n";
            if($CLIENTS{$rh}->binary()) # file transfer
            {
              #print "binary socket\n";
              my $text={
                file=>{
                  hash=>$CLIENTS{$rh}->filehash(),
                  name=>$CLIENTS{$rh}->filename(),
                  size=>$CLIENTS{$rh}->filesize()
                  #transferto=>$CLIENTS{$rh}->filetransferto()
                }
              };
              #print "message sent";
              my $m=Message->new($CLIENTS{$rh}->ip,$CLIENTS{$rh}->port,$CLIENTS{$rh}->socket,$text, $message);
              push @messages, $m;
              next;
            }
            my @obj;
            eval {
              my $json=new JSON;
              $json=$json->utf8(1);
              @obj= $json->decode($message);
              };
            if(!catch my $e) # no errors in json message
            {
              my $o=$obj[0][0];
              if($MODE eq 'client')
              {
                my @m=_process_client_mode($o, $rh, $message);
                push @messages, @m if @m;
              }
              else{ # Master mode
                my @m=_process_server_mode($o,$rh,$message);
                push @messages, @m if @m;                
              } # Any mode
              my @m=_process_both_mode($o,$rh,$message);
              push @messages,@m if @m;
            }
            else # json string is invalid
            {
              print STDERR RED, "Mailformed packet buf:($buf)\nmessage: ($message)\n$!\n",RESET;
              #warn "Mailformed packet buf:($buf)\nmessage: ($message)\n$!\n";
            }
          }
        }
        if(time-$LAST_PEER_PROCESSING_TIME>2) # call every 2 seconds
        {
          #some connections routine
          peers_processing();
        }
      }
    }
  }
  return @messages;
}
1;
