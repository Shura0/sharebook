#!/usr/bin/perl -w

use Test::More tests => 5;

require_ok('db_routine.pm');
require_ok('net_routine.pm');
require_ok('pdf_routine.pm');
require_ok('fb2_routine.pm');
require_ok('peer.pm');
