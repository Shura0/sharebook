
package network;
use v5.10;
use utf8;
use IO::Socket;
use IO::Select;
use JSON;
use Exception::Class::TryCatch;

# TODO: remove global variables

my $SELECT;
my $SOCKET;
my $BUFFER;
my $OUTBUFFER;
my @MESSAGES;

my $CONNECTED;
my %CLIENTS;

sub _connect
{
  my $host=shift;
  my $port=shift;
  $SOCKET=0;
  print "trying to connect to daemon on port $port\n";
  $sock=new IO::Socket::INET (
  PeerAddr => $host,
  PeerPort => $port,
  Proto => 'tcp',
  Blocking => 0);
  $CONNECTED=0;
  $OUTBUFFER="";
  $BUFFER="";
  if(!$sock)
  {
    print "Could not create socket: $!n";
    return undef;
  }
  #binmode $sock, ":utf8";
  $SELECT = new IO::Select();
  $SELECT->add($sock);
  my $client=Peer->new;
  $client->socket($sock);
  $client->ip($host);
  $client->port($port);
  $client->type("GUI");
  $CLIENTS{$sock}=$client;
  return $sock;
}

sub connect
{
  my $host=shift;
  my $port=shift;
  return if not $host;
  return if not $port;
  return if $SOCKET;
  $SOCKET=_connect($host, $port);
  $CONNECTED=0;
  return $SOCKET;
}

#event when socket is connected or disconnected
sub onConnect
{
  my $status=shift;
  if($status)#connected
  {
    print "Connected\n";
    $CLIENTS{$SOCKET}->send("[{\"get\":\"peers\"}]\n");
  }
}

sub checkConnection 
{
  return if ! $SOCKET->connected();
  if($SOCKET->connected() ne $CONNECTED)
  {
    $CONNECTED=$SOCKET->connected();
    onConnect($CONNECTED);
  }
}

sub connected
{
  return $SOCKET->connected() if($SOCKET);
  return 0;
  
}

sub send
{
   my $message=shift;

   if($SOCKET)
   {
      $CLIENTS{$SOCKET}->send($message);
   }
}

sub get_data
{
  ($SOCKET and $SELECT)or return;
  my @messages;
  checkConnection();
  while(my @ready = $SELECT->can_read(0.1)) {
    foreach my $rh (@ready) {
      my $buf;
      my $message;
      if(!sysread($rh, $buf, 1024))
      {
        print "Client closed connection\n";
        $SELECT->remove($rh);
        close($rh);
        delete $CLIENTS{$rh};
        $SOCKET=0;
        next;
      }
      else{
        if($CLIENTS{$rh}->add_buffer($buf))
        {
          while ($message=$CLIENTS{$rh}->pop_buffer)
          {
            my @obj;
            eval {
              my $json=new JSON;
              $json=$json->utf8(1);
              @obj= $json->decode($message);
              };
            if(!catch my $e)
            {
              my $o=$obj[0][0];
              push @messages,$o;
            }
          }
        }
      }
    }
  }
  return @messages;
}

1;