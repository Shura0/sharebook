package cfg;

use File::HomeDir;

use Data::Dumper;
use File::Path qw(make_path);

my $FILENAME="config";
# config file is ~/.config/sharebook/config

my $CONFIG; # full text of config

my $PATH;
if($^O=~/win/i)
{
  $PATH='\\Application Data\\sharebook\\';
}
else
{
  $PATH='/.config/sharebook/';
}


sub parse_config
{
  my $filename=shift or return 0;
  my $cfg={};
  open $fh, "<$filename" or (warn "config can not be read\n" and return 0);
  while (my $str=<$fh>)
  {
    $str=~s/\n//;
    my @str=split / *= */,$str;
    $cfg->{$str[0]}=$str[1];
  }
  close $fh;
  return $cfg;
}

sub configdir
{
  return File::HomeDir->my_home.$PATH;
}

sub homedir
{
  return File::HomeDir->my_documents;
}

sub filename
{
  return File::HomeDir->my_home.$PATH.$FILENAME;
}

sub read
{
  my $filename=File::HomeDir->my_home.$PATH.$FILENAME;
  my $config;
  print "read file $filename\n";
  if( not -e $filename)
  {
    if( not -d (File::HomeDir->my_home.$PATH))
    {
      make_path(File::HomeDir->my_home.$PATH) or die "Can't create dir '".File::HomeDir->my_home.$PATH;
    }
    open $fh ,">$filename";
    close $fh;
    return undef;
  }
  print "config opened for reading\n";
  $config=parse_config($filename);
  return $config;
}

sub write
{
  my $cfgfile=File::HomeDir->my_home.$PATH.$FILENAME;
  my $data=(shift or $CONFIG);
  print $cfgfile." ";
  open $fh, ">$cfgfile" or (warn "can't be written\n" and return 0);
  print "opened for writing successfuly\n";
  foreach my $str (keys %{$data})
  {
    print $fh "$str = ".$data->{$str}."\n";
  }
  close $fh;
  return 1;
}

sub set_config
{
  $CONFIG=shift;
}
1;