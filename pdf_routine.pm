
#pdf_routine

use strict;
use PDF::API2;
use Exception::Class::TryCatch;

sub parse_pdf #filename
{
	my $filename=shift;
	my %info;
	eval{
	my $pdf = PDF::API2->open($filename);
	%info=$pdf->info;
	};
	if(!catch my $e)
	{
		my @author=split / /,$info{Author};
		return ($author[0],$author[1],$info{Title});
	}
	return undef;
}
1;